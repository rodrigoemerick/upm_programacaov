using Microsoft.VisualStudio.TestTools.UnitTesting;
using Calculator;

namespace CalculatorTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestSum()
        {
            int n1 = 20, n2 = 10;
            Calculator.Calculator calculator = new Calculator.Calculator();

            int result = calculator.Sum(n1, n2);

            Assert.AreEqual(30, result);
        }

        [TestMethod]
        public void DivInt()
        {
            int n1 = 20, n2 = 10;
            Calculator.Calculator calculator = new Calculator.Calculator();

            int result = calculator.Div(n1, n2);

            Assert.AreEqual(2, result);
        }

        [TestMethod]
        public void DivFloat()
        {
            float n1 = 20.0f, n2 = 10.0f;
            Calculator.Calculator calculator = new Calculator.Calculator();

            float result = calculator.Div(n1, n2);

            Assert.AreEqual(2.0f, result);
        }
    }
}
