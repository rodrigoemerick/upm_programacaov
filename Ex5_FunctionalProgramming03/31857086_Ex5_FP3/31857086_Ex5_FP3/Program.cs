﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace _31857086_Ex5_FP3
{
    class Program
    {
        static void Main(string[] args)
        {
            Func<int, bool> IsEven = n => n % 2 == 0;

            var numbers = Enumerable.Range(1, 10);

            var evenNumbers = WhereImplementation(numbers, IsEven);

            Console.WriteLine("Evens in sequence from 1 to 10: ");
            foreach (Option<int> num in evenNumbers)
            {
                Console.WriteLine("Even: " + num);
            }

            Console.ReadKey();
        }

        static IEnumerable<Option<T>> WhereImplementation<T>(IEnumerable<T> enumerable, Func<T, bool> predicate) where T : struct
        {
            foreach (T element in enumerable)
            {
                if (predicate(element))
                {
                    yield return element;
                }
            }
        }
    }

    //Option implementation
    public static class FP
    {
        public static None None => None.Value;

        public static Option<T> Some<T>(T value) => new Some<T>(value);
    }

    public struct None
    {
        public static readonly None Value = new None();
    }

    public struct Some<T>
    {
        public T Value { get; }
        public Some(T value)
        {
            if (value == null) throw new ArgumentNullException();
            Value = value;
        }
    }

    public struct Option<T>
    {
        readonly T Value;
        readonly bool IsSome;

        private Option(T value)
        {
            Value = value;
            IsSome = true;
        }

        public static implicit operator Option<T>(None _) => new Option<T>();
        public static implicit operator Option<T>(Some<T> some) => new Option<T>(some.Value);
        public static implicit operator Option<T>(T value) => value == null ? FP.None : FP.Some(value);
        public TResult Match<TResult>(Func<TResult> None, Func<T, TResult> Some) => IsSome ? Some(Value) : None();

        public override string ToString() => IsSome ? $"{Value}" : "Option.None";
    }
}