﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace _31857086_Ex4_FP2
{
    public static class FP
    {
        public static None None => None.Value;

        public static Option<T> Some<T>(T value) => new Some<T>(value);
    }

    public struct None
    {
        public static readonly None Value = new None();
    }

    public struct Some<T>
    {
        public T Value { get; }
        public Some(T value)
        {
            if (value == null) throw new ArgumentNullException();
            Value = value;
        }
    }

    public struct Option<T>
    {
        readonly T Value;
        readonly bool IsSome;

        private Option(T value)
        {
            Value = value;
            IsSome = true;
        }

        public static implicit operator Option<T>(None _) => new Option<T>();
        public static implicit operator Option<T>(Some<T> some) => new Option<T>(some.Value);
        public static implicit operator Option<T>(T value) => value == null ? FP.None : FP.Some(value);
        public TResult Match<TResult>(Func<TResult> None, Func<T, TResult> Some) => IsSome ? Some(Value) : None();

        public override string ToString() => IsSome ? $"{Value}" : "Option.None";
    }

    public class Program
    {
        enum Names { Carlos, Roberto, Maria, João, Ricardo };

        static void Main(string[] args)
        {
            //Testes dos métodos
            Console.WriteLine("Método 1: \n");

            string name1 = "Carlos";
            string name2 = "Rodrigo";
            Option<Names> n1 = name1.CheckIfEnumExists<Names>();
            Option<Names> n2 = name2.CheckIfEnumExists<Names>();

            Console.WriteLine("Result for name 1 is: " + n1 + "\nResult for name 2 is: " + n2);

            Console.WriteLine();

            Console.WriteLine("Método 2: \n");
            string str1 = "100", str2 = "Oi";
            Option<int> testStr1 = str1.StringToInt();
            Option<int> testStr2 = str2.StringToInt();

            Console.WriteLine("Result string 1: " + testStr1 + "\n" + "Result string 2: " + testStr2);

            Console.WriteLine();

            Console.WriteLine("Método 3: \n");

            Option<string> someString = "Emerick", noneString = FP.None;
            string str3 = OptionalString(someString), str4 = OptionalString(noneString);

            Console.WriteLine("SomeString has: " + str3 + "\n" + "NoneString has: " + str4);
        }

        public static Func<Option<string>, string> OptionalString = str => str.Match(() => "Hello, World!", (value) => $"Hello, {value}!");
    }

    public static class Extensions
    {
        //Método 1
        public static Option<T> CheckIfEnumExists<T>(this String str) where T : struct
        {
            T name;
            bool canParse = Enum.TryParse(str, out name); 
            Option<T> optEnum;

            if (canParse)
            {
                optEnum = name;
            }
            else
            {
                optEnum = FP.None;
            }

            return optEnum;
        }

        //Método 2
        public static Option<int> StringToInt(this String str)
        {
            int result;
            bool canParse = int.TryParse(str, out result);
            Option<int> optInt;

            if (canParse)
            {
                optInt = result;
            }
            else
            {
                optInt = FP.None;
            }

            return optInt;
        }
    }
}
