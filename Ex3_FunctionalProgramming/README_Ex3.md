# Paradigma Funcional x Paradigma Orientado a Objetos  
## Vantagens do Paradigma Funcional  
A programação funcional possui as vantagens de trabalhar muito melhor com códigos mais limpos e corretos, visto que ela foca mais em trabalhar com funções matemáticas, levando ao uso maior de funções puras, honestas e que não alteram o estado global do código. Pode-se ter na programação funcional maior certeza que será recebido o resultado esperado de uma função. Este paradigma também facilita testes de suas funções.

## Desvantagens do Paradigma Funcional
Agora as desvantagens deste paradigma são: alocamento maior de memória, visto sua característica de imutabilidade de suas variáveis e de estado do código; necessidade de escrever uma função para cada tipo de cálculo que é desejado ser feito.