﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Ex3_FP
{
    class Program
    {
        static void Main(string[] args)
        {
            //EXERCICIO 1
            Console.WriteLine("Exercício 1: ");
            int[] nums = { 0, 5, 10 };

            Func<int, bool> greaterThanFive = value => value > 5;

            var tests = nums.Where(greaterThanFive.negatePredicate());

            Console.Write("Negated predicates results: ");
            foreach (int num in tests)
            {
                Console.Write($"{num} ");
            }

            Console.WriteLine("\n\nExercício 2: ");

            //EXERCICIO 2
            List<int> m_List = new List<int> { 2, 12, 34, 52, 10, 76, 21, 93, 100, 200, 139, 29, 3 };
            List<int> m_ListCopy;

            m_ListCopy = SortList(m_List);

            Console.Write("Values: ");
            foreach (int value in m_ListCopy)
            {
                Console.Write($"{value} ");
            }

            Console.WriteLine("\n\nExercício 3: ");

            //EXERCICIO 3
            string single = "roberto";
            string multiple = "roberto henrique";

            string s = single.ReturnNewString();
            string m = multiple.ReturnNewString();

            Console.WriteLine("Single String: " + s + " " + "Multiple String: " + m);
            Console.ReadKey();
        }

        static List<int> SortList(List<int> listToSort)
        {
            List<int> copyList = new List<int>();

            foreach (int num in listToSort)
            {
                copyList.Add(num);
            }

            return QuickSort(copyList, 0, copyList.Count - 1);
        }

        //-------------------------QUICK SORT--------------------------
        static List<int> QuickSort(List<int> list, int left, int right)
        {
            int pivot;

            if (left < right)
            {
                pivot = Partition(list, left, right);
                if (pivot > 1)
                {
                    QuickSort(list, left, pivot - 1);
                }
                if (pivot + 1 < right)
                {
                    QuickSort(list, pivot + 1, right);
                }
            }

            return list;
        }

        static int Partition(List<int> list, int left, int right)
        {
            int pivot;
            pivot = list[left];

            while (true)
            {
                while (list[left] < pivot)
                {
                    ++left;
                }
                while (list[right] > pivot)
                {
                    --right;
                }

                if (left < right)
                {
                    int temp = list[right];
                    list[right] = list[left];
                    list[left] = temp;
                }
                else
                {
                    return right;
                }
            }
        }
    }
    public static class Extensions
    {
        public static Func<T, bool> negatePredicate<T>(this Func<T, bool> funcToNegate) => t => !funcToNegate(t);

        public static string ReturnNewString(this String str)
        {
            string[] newStr = str.Split();
            string newStrFinalWord = newStr[newStr.Length - 1];

            Func<string, bool, string> firstLetterUpperCase = (s, b) => b ? s.Substring(0, 1).ToUpper() + ". " : s.Substring(0, 1).ToUpper();
            Func<string, string> otherLettersLowerCase = s => s.Substring(1, s.Length - 1).ToLower();
            Func<string, string, string> finalWord = (str1, str2) => firstLetterUpperCase(newStrFinalWord, false) + otherLettersLowerCase(newStrFinalWord);

            if (newStr.Length > 1)
            {
                List<string> abrWords = new List<string>();
                for (int i = 0; i < newStr.Length - 1; ++i)
                {
                    abrWords.Add(firstLetterUpperCase(newStr[i], true));
                }

                string wordsExceptLast = string.Join("", abrWords.ToArray());

                string final = finalWord(newStrFinalWord, newStrFinalWord);

                return wordsExceptLast + final;
            }
            else
            {
                string final = finalWord(newStrFinalWord, newStrFinalWord);

                return final;
            }
        }
    }
}