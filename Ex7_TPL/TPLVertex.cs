﻿using UnityEngine;
using System.Threading.Tasks;

public class TPLVertex : MonoBehaviour
{
    void Awake()
    {
        Mesh mesh = GetComponent<MeshFilter>().mesh;

        Vector3[] vertices = mesh.vertices;

        Parallel.For(0, vertices.Length, (i) =>
        {
            vertices[i].x += Random.Range(0.0f, 1.0f);
            vertices[i].y += Random.Range(0.0f, 1.0f);
            vertices[i].z += Random.Range(0.0f, 1.0f);
        });

        mesh.vertices = vertices;
        mesh.RecalculateNormals();
    }
}